using System;
using System.Threading.Tasks;
using Core.Contracts;
using Core.Services;
using MassTransit;

namespace NotificationService.Consumers
{
    public class EmailConsumer : IConsumer<EmailContract>
    {
        private readonly IMailService _mailService;

        public EmailConsumer(IMailService mailService)
        {
            _mailService = mailService;
        }
        
        public Task Consume(ConsumeContext<EmailContract> context)
        {
            _mailService.SendEmail(context.Message.ReceiverEmail);
            return Task.CompletedTask;
        }
    }
}