namespace Core.Services
{
    public interface IMailService
    {
        void SendEmail(string to);
    }
}