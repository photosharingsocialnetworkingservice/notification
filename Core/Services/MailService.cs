using System;
using System.Net;
using System.Net.Mail;
using System.Threading.Tasks;

namespace Core.Services
{
    public class MailService : IMailService
    {
        private readonly SmtpClient _client;
        
        public MailService()
        {
            var emailLogin = Environment.GetEnvironmentVariable("EMAIL_LOGIN");
            var emailPassword = Environment.GetEnvironmentVariable("EMAIL_PASSWORD");
            
            _client = new SmtpClient("smtp.gmail.com", 587)
            {
                EnableSsl = true,
                DeliveryMethod = SmtpDeliveryMethod.Network,
                UseDefaultCredentials = false,
                Credentials = new NetworkCredential(emailLogin, emailPassword)
            };
        }

        public void SendEmail(string to)
        {
            var mail = new MailMessage("pssns.inz@gmail.com", to)
            {
                Subject = "Registration on Lamini",
                Body = "Registration is successful"
            };
            _client.Send(mail);
        }
    }
}